Kodak Image Suite comparison data for:

- Marc Olano, Dan Baker, Wesley Griffin, Joshua Barczak, "Variable Bit Rate
  GPU Texture Decompression", EGSR 2011: Proceedings of the Eurographics
  Symposium on Rendering, (Prague, Czech Republic, June 27-29, 2011).
  http://www.csee.umbc.edu/olano/papers/texcompress.pdf

The `*_stats.csv` files contain the mean SSIM and SHAME-II comparison values
for each compressed image as aggregated in Figure 9 in the paper.

The directories:

- `lossless`
- `bc7`
- `dxt1`
- `lossy1`
- `lossy6`
- `lossy7`
- `jp2k_3bpp`
- `jp2k_1.5bpp`

contain PNG files after compressing and then decompressing. This preserves the
compression artifacts but makes comparing images much more simple.

In the paper, we refer to VBR vX algorithms, where X is 1, 2, or 3.

- The VBR v1 algorithm corresponds to the `lossy1` directory.
- The VBR v2 algorithm corresponds to the `lossy6` directory.
- The VBR v3 algorithm corresponds to the `lossy7` directory.

The `overcompression` directory and `overcompression_stats.csv` file are the
mean SSIM and SHAME-II metrics for Figure 3.

The `tradeoffs` directory contains metric results for Figure 13.

Contact:

Marc Olano <olano@umbc.edu>

Wesley Griffin <wesley.griffin@nist.gov>

Last updated: 7 December 2015

